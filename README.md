# Middleware Engineering "Document Oriented Middleware using MongoDB"

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Implementierung
### MongoDB-Installation
MongoDB habe ich unter Windows im WSL(Windows Subsystem for Linux) installiert und erfolgreich getestet.    
WSL ist eine Linux-Schicht für Windows, die es erlaubt Linux-Befehle auszuführen. Mehr dazu [hier](https://docs.microsoft.com/en-us/windows/wsl/install-win10). Dieses Subsystem ist nicht ganz perfekt, aber es finden sich oft Workarounds um nicht offiziel unterstütze Linux-Pakete lauffähig zu machen.    
Nach der Installation bekommt man die Möglichkeit eine Bash-Konsole zu öffnen, um auf das Subsystem zugreifen zu können.
In dieser kann man wie folgt MongoDB installieren:

```bash
cd ~
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
sudo apt-get update
sudo apt-get install -y mongodb-org
cd ~
sudo mkdir -p data/db
```
Danach startet man einfach den Service mit folgenden Befehl:
```bash
sudo mongod --dbpath ~/data/db
```

### Bestehenden Code implementieren
Ich habe die bestehenden Projekte reinkopiert und folgende  
Ordnerstruktur erstellt:
* Windanlage -> einzelne Windanlage
* Windpark -> Zentrale für die Windanlagen

Die neue Aufgabe wird unter **Windzentrale** zu finden sein.

### Ausführen des Programmes  
Für Windows habe ich ein einfaches Batch-Skript `run.bat` geschrieben, mit den man einfach 6 Windanlangen und 2 Parks ausführt, 3 Windanlagen gehören jeweils zu einem Park. Am Schluss wird die Windzentrale gestartet. Jeder, der das Skript benutzen möchte, sollte persönlich den Pfad zur ApacheMQ im Skript für sein System anpassen.

Man kann aber auch manuell die Programme ausführen:

Mit folgenden Befehl kann der ApacheMQ-Broker in **meinem System** gestartet werden:
```bash
C:\Programme_etc\apache-activemq-5.15.3\bin\activemq start
```

Windanlagen können befolgt gestartet werden:
```bash
mvn spring-boot:run -D"spring-boot.run.arguments"=--server.port=8090,--windengine.id=3,--windpark.id=0
```
Wobei ich dazu Maven zum *PATH* hinzufügen musste und den Spring Parameter unter Anführungszeichen geben musste. Mit dem Serverport gibt man an, auf welchen Port die Anlage rennen soll, die windengine.id ist die ID der jeweiligen Anlage, windpark.id ist die ID des dazugehörigen Windparks.

Windparks sind durch eigene IDs von einander unterscheidbar, dadurch muss auch bei mehreren der Port gesetzt werden.
Der Windpark kann einfach mit folgenden Befehl gestartet werden:
```bash
mvn spring-boot:run -D"spring-boot.run.arguments"=--server.port=8090,--windpark.id=0	
```

Die Windzentrale kann mit folgenden Befehl gestarte werden:  

```java
start mvn spring-boot:run -D"spring-boot.run.arguments"=--server.port=9999,--windpark.count=2
```

Der Parameter `windpark.count` muss angegeben werden, damit genügen Threads für alle Windparks zur Verfügung stehen.

### Rest Consuming

Zuerst habe ich bestehende Daten-Modelle aus der MOM-Aufgabe ins Windzentrale-Projekt kopiert und Rest-Consuming nach der Spring-Dokumentation ausprobiert. Vorher habe ich noch Windanlagen bzw. Parks gestartet.
```java
@SpringBootApplication
public class ZentraleApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZentraleApplication.class, args);
        System.out.println("Hallo!");
        RestTemplate rest = new RestTemplate();
        WindparkData data = rest.getForObject("http://localhost:8080/windpark/data_json", WindparkData.class);
        System.out.println(data.toString());
    }
}
```
Output:
```
Hallo!
0
Windengine Info: ID = 0, timestamp = 2019-03-05 10:21:03.029, windspeed = 13,320000
Windengine Info: ID = 1, timestamp = 2019-03-05 10:21:02.178, windspeed = 14,780000
Windengine Info: ID = 2, timestamp = 2019-03-05 10:21:03.002, windspeed = 32,190000
```
Somit war ich von der Funktionalität überzeugt.

### RestService

Als nächstes folgte die saubere Implementierung eines REST-Consumers für die Windzentrale. Ich habe ähnlich wie bei der MOM-Aufgabe einen Service in der Klasse `RestService` erstellt, der für jeden Windpark einen Thread `RestConsumer` erstellt. Dieser "konsumiert" per REST Daten von den Windparks und übergibt einen als Attribut gespeicherten Mongo-Objekt (`MongoSaver`, näheres gleich) die Daten. Alle drei Sekunden werden neue Daten eingeholt.

### MongoSaver

Um in MongoDB Objekte zu speichern, benötigt man eine "Entität" dafür. Diese ist mit der Klasse `WindparkData` schon vorhanden ist. Als nächstes braucht mein ein Repository (`WindparkDataRepository`). Diese Klasse wird gebraucht um CRUD-Operationen wie Speichern bzw. Abfragen durchführen zu können.

Die Klasse `MongoSaver` ist ein Thread, der als Parameter eine Liste besitzt, in die die `RestConsumer`-Threads Daten ablegen, die in der Datenbank abgespeichert werden sollen. Solange Daten sich in der Liste befinden, werden diese abgearbeitet und alle nach der Reihe in MongoDB abgespeichert.

Mit der Zeit sind das schon ziemlich viele Daten:

![MongoDB - Shell Output](img/mongo.PNG)    
*MongoDB - Shell Output*     

Die Datenstruktur der Zentrale (`WindzentraleData`) ist im Grunde eine Map bzw. Dictionary, wobei der Key für einen Windpark steht, während der dazugehörige Value eine Liste aus `WindengineData` ist, also eine Liste aus Windanlagen-Datensätzen. Diese werden nach der Zeit geordnet, neuste Einträge stehen am Anfang.

### Webanzeige

Für eine einfache Webanzeige, habe ich einfach REST-Schnittstellen implementiert, die die Datenbank als JSON ([localhost:9999/windzentrale/data_json](localhost:9999/windzentrale/data_json)) bzw. XML ([localhost:9999/windzentrale/data_xml](localhost:9999/windzentrale/data_xml)) ausgibt. Die Daten werden direkt aus der Datenbank geholt und zu den beiden Typen geparst. Bei der XML-Ausgabe hatte ich große Probleme, nachdem ich aber die Dependencies auf ein Minimum reduziert habe, funktionierte diese endlich. 

Für die eigentliche Webanzeige habe ich eine einfache HTML-Seite mit Tabelle erstellt, die per JavaScript von der JSON-REST-Schnittstelle die Daten holt und mit diesen dann die Tabelle füllt.  Leider gibt es keine wirklich schöne Lösung um JSON in eine Tabelle umzuwandeln, man muss einfach mit Schleifen Strings mit Datensätzen bauen und dieser der Tabelle hinzufügen. Dafür fand ich [hier](https://stackoverflow.com/questions/51275730/populate-html-table-with-json-data) eine gelunge Hilfestellung. 

![Webanzeige - localhost:9999/](img/web.PNG)   
*Webanzeige - localhost:9999/*

## Anwendungsfall

Es soll die maximale Windgeschwindigkeit einer Windanlage in einen gewissen Zeitraum abgefragt werden. 

Den Timestamp muss man manuell eingeben, dieser muss in der Vergangenheit liegen. Will ich den Maximalwert der letzten Stunde bekommen, muss der Timestamp von jetzt minus eine Stunde eingegeben werden. 

```sql
db.windparkData.aggregate([
	{"$match":{"data": {"$elemMatch": {"timestamp": {"$gte": "2019-04-01 11:10.10.000"}}}}},
	{ "$unwind": "$data" },
	{ "$group": { "_id": "null", "max": { "$max": "$data.windspeed" } } },
	{ "$project": { "max": 1, "_id":0 } }
])
```

![](img/fragestellung_2.PNG)

Will man jetzt herausfinden welche Windanlage diesen Maximalwert besitzt, kann man einfach mit einer zweiten Query nach ihr filtern.

```sql
db.windparkData.find({"data": {"$elemMatch": {"windspeed": {"$eq": 80.92}}}})
```

## Fragestellungen

- **Nennen Sie 4 Vorteile eines NoSQL Repository im Gegensatz zu einem relationalen DBMS**    

  * Schneller produktionsbereit, denn es werden keine zeitaufwendigen Schemas/Tabellen und damit komplexe Designentwürfe benötigt
  * NoSQL basiert meist auf semistrukturierten Daten (~ CSV, XML, JSON), somit hat man mehr Freiheit auf Bezug wie Objekte gestaltet werden können.
  * Viele NoSQL-Vertreter benutzen eine auf JSON basierte Struktur, dies erleichtert den Zugriff und Verarbeitung in beispielsweise Web-Anwendungen, da schon eine große Anzahl an JSON-Methoden existieren.
  * NoSQL-Systeme sind meist sehr gut in Bezug auf Skalierbarkeit, man kann problemlos große Datenmengen speichern, viele System bieten auch eine einfache Konfiguration für Replikation. Auch ist der Lese/Schreiben Durchsatz meist für Big Data optimiert (Stickwort *MapReduce*).

- **Nennen Sie 4 Nachteile eines NoSQL Repository im Gegensatz zu einem relationalen DBMS**    

  * NoSQL bietet keine so einheitliche Syntax wie relationale DBMS, die meist auf SQL basieren. Die unterschiede verschiedener SQL-Dialekte sind meist marginal.
  * In NoSQL ist es schwierig bis unmöglich wirklich Relationen zwischen Objekten zu realisieren.
  * Nicht wirklich geeignet für Transaktionen, da meist nur eine *BASE* und keine *ACID* Konsistenz erreicht werden kann.
  * NoSQL sind eine eher jüngere Entwicklung in Vergleich zu relationalen System, somit kann die Unterstützung bzw. der Support derzeit noch weniger umfangreich ausfallen.

- **Welche Schwierigkeiten ergeben sich bei der Zusammenführung der Daten?**     
  Beim Zusammenführen der Daten wollte ich auf eine schöne Struktur kommen.  Dabei kam ich auf die Idee eine Map bzw. ein Dictionary als Grundstruktur zu benutzen. Jeder Windpark wäre ein Key, der als Value eine Liste von Windanlage-Daten besitzt. Die Umsetzung erwies sich schwerer als gedacht. MongoDB macht diese Konversion automatisch, gibt man der Datenbank einfach Windpark-Daten, bildet die Datenbank von selbst diese Struktur, das Problem ist es in Java diese Struktur aufrecht zu erhalten. Ich entwurf einen Datentyp `WindzentraleData` mit der HashMap. Die Konvertierung der Daten aus der Datenbank zum Datentypen funktionierten sogar ganz gut, sogar JSON lies sich grob über eine REST-Schnittstelle auslesen. Problem war aber der XML-Parser, der konnte einfach kein gültiges Element daraus erstellen. Im Internet fand ich heraus, dass man dafür `MapAdapter` benötigt, diese halfen auch der JSON-Struktur, XML ging aber noch immer nicht. Ich probierte den Ansatz, nicht den Annotation-Marshaller vom REST-Controller zu benutzen, sondern baute einen mit Hilfe von JAXB-Methoden und gab bei der Restschnittstelle nur den davon erstellten String zurück. Dies funktionierte und brachte mich auf die Vermutung, dass vielleicht die Dependencies für die `produces`-Annotation einfach Probleme machten. Ich löschte die meisten Dependencies, teils waren noch alte von vorherigen Projekten vorhanden und auf einmal funktionierte die XML-Ausgabe. Ich war erleichtert.

- **Mit welchem Befehl koennen Sie die Windgeschwindigkeit von Windkraftanalage 1 vom Windpark 1 in der Shell auf 0 setzen?**   
  Zuerst muss das Element gefunden werden, dabei notiere ich mir den Timestamp.

  ```sql
  db.windparkData.find({"windparkID": "1"}, {"data": { $elemMatch: {"windengineID": "1"}}}).sort({"timestamp": 1}).limit(1)
  ```

  Output:

  ```sql
  { "_id" : ObjectId("5c7e5054da73642470043d26"), "data" : [ { "windengineID" : "1", "timestamp" : "2019-03-05 11:32:51.376", "windspeed" : 10.87, "unitWindspeed" : "kmH", "temperature" : -38.82, "unitTemperature" : "C", "power" : 1119.41, "unitPower" : "kwH", "blindpower" : 36.81, "unitBlindpower" : "kwH", "rotationspeed" : 121.17, "unitRotationspeed" : "uM", "bladeposition" : 25, "unitBladeposition" : "grad" } ] }
  ```

  Windspeed auf 0 setzen:

  ```sql
  db.windparkData.update({"windparkID": "1","data.timestamp": "2019-03-05 11:32:51.376"}, {$set: {"data.$.windspeed":0}})
  ```

  Es wird `$set` benötigt, da `windspeed` im `data`-Array ist.

  

## Quellen
* [Bash - Parellisierung](https://stackoverflow.com/questions/11010834/how-to-run-multiple-dos-commands-in-parallel)
* [Spring - Access Properties](https://stackoverflow.com/questions/30528255/how-to-access-a-value-defined-in-the-application-properties-file-in-spring-boot)
* [JAXB - Property Order](https://stackoverflow.com/questions/5435138/jaxb-and-property-ordering)
* [Markdown - Strikethrough-Text](https://webapps.stackexchange.com/questions/24394/is-it-possible-to-use-strikethrough-s-strike-del-in-trello)
* [MongoDB - Installation](https://gist.github.com/Mikeysax/cc86c30903727c556bcce960f7e4d59b)
* [Spring - REST Consuming](https://spring.io/guides/gs/consuming-rest/)
* [Spring - MongoDB](https://spring.io/guides/gs/accessing-data-mongodb/)
* [JAXB](https://stackoverflow.com/questions/6768544/jaxb-class-has-two-properties-of-the-same-name)
* [JAXB - Map](http://blog.bdoughan.com/2013/03/jaxb-and-javautilmap.html)
* [JSON zu Tabelle](https://stackoverflow.com/questions/51275730/populate-html-table-with-json-data)
* [MongoDB - Query](https://docs.mongodb.com/manual/tutorial/query-arrays/)
* [SQL v NoSQL](https://stackoverflow.com/questions/4160732/nosql-vs-relational-database)
* [MongoDB - Find Max](<https://stackoverflow.com/questions/32076382/mongodb-how-to-get-max-value-from-collections>) 
* [MongoDB - Find Date](<https://stackoverflow.com/questions/39184546/mongodb-date-range-query-for-past-hour>)
