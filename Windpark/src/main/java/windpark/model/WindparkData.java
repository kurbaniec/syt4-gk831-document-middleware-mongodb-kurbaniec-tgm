package Windpark.model;

import org.springframework.beans.factory.annotation.Value;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;

/**
 * @author Kacper Urbaniec
 * @version 21.01.2019
 */
@XmlRootElement(name = "Windpark")
@XmlType(propOrder={"windparkID", "windpark"})
public class WindparkData {

    @XmlElement(name = "WindparkID")
    private String windparkID;

    @XmlElement(name = "WindengineData")
    private WindengineData[] data;

    public WindparkData(String windparkID, WindengineData[] data) {
        this.windparkID = windparkID;
        this.data = data;
    }

    public WindparkData() {}

    public String getWindparkID() {
        return windparkID;
    }

    public WindengineData[] getWindpark() {
        return data;
    }

    public void setWindparkID(String windparkID) {
        this.windparkID = windparkID;
    }

    public void setWindpark(WindengineData[] data) {
        this.data = data;
    }
}
