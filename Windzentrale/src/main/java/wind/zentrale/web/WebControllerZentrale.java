package wind.zentrale.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

/**
 * @author Kacper Urbaniec
 * @version 19.03.2019
 */
@Controller
public class WebControllerZentrale {

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/")
    public String main() {
        return "main.html";
    }
}
