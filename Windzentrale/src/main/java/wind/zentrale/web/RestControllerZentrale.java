package wind.zentrale.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import wind.zentrale.model.*;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.*;

/**
 * @author Kacper Urbaniec
 * @version 08.03.2019
 */
@RestController
public class RestControllerZentrale {

    @Autowired
    WindparkDataRepository repository;

    @Value("${windpark.count}")
    private String windCountString;

    // Gespeicherte Daten als XML ausgeben
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/windzentrale/data_xml", produces = MediaType.APPLICATION_XML_VALUE)
    public WindzentraleData windparkDataXML() {
        return dataBuilder();
    }

    // Gespeicherte Daten als XML ausgeben | Alternative
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/windzentrale/data_xml2", produces = MediaType.APPLICATION_XML_VALUE)
    public String windparkDataXML2() {
        return dataBuilderXML();
    }


    // Gespeicherte Daten als JSON ausgeben
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/windzentrale/data_json", produces = MediaType.APPLICATION_JSON_VALUE)
    public WindzentraleData windparkDataJSON() {
        return dataBuilder();
    }

    private WindzentraleData dataBuilder() {
        HashMap<String, ArrayList<WindengineData>> zentrale = new HashMap<>();
        int windcount = Integer.parseInt(windCountString);
        for(int i = 0; i < windcount; i++) {
            List<WindparkData> wp = repository.findByWindparkID(""+i);
            ArrayList<WindengineData> we = new ArrayList<>();
            for(WindparkData data: wp) {
                we.addAll(Arrays.asList(data.getWindpark()));
            }
            Collections.sort(we);
            zentrale.put(""+i, we);
        }
        return new WindzentraleData(zentrale);
    }

    private String dataBuilderXML() {
        WindzentraleData zentrale = dataBuilder();
        String xml = "";
        try {
            StringWriter sw = new StringWriter();
            JAXBContext jaxbContext = JAXBContext.newInstance(WindzentraleData.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.marshal(zentrale, sw);
            xml = sw.toString();
            System.out.println(xml);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return xml;
    }



}
