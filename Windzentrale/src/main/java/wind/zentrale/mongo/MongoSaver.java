package wind.zentrale.mongo;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import wind.zentrale.model.WindengineData;
import wind.zentrale.model.WindparkData;
import wind.zentrale.model.WindparkDataRepository;
import wind.zentrale.rest.RestService;

import java.util.LinkedList;


/**
 * Speichert alle in der Warteschlange stehende Daten in MongoDB ab.
 * <br>
 * Die Daten werden mittels {@link wind.zentrale.rest.RestConsumer}
 * eingeholt, der {@link MongoSaver#addData(WindparkData)} aufruft,
 * um die Daten zu übermitteln.
 * <br>
 * @author Kacper Urbaniec
 * @version 08.03.2019
 */
@Component
@Scope("prototype")
public class MongoSaver implements Runnable{
    @Autowired
    private ApplicationContext context;

    @Autowired
    private RestService service;

    @Autowired
    private WindparkDataRepository repository;

    private boolean sending =  true;

    private LinkedList<WindparkData> stack;

    public MongoSaver() {
        stack = new LinkedList<>();
    }

    @Override
    public void run() {
        while (sending) {
            try {
                //System.out.println("baumBaumBaum");
                Thread.sleep(100);
                while (!stack.isEmpty()) {
                    WindparkData el = stack.pollFirst();
                    if (el != null)
                        repository.save(el);
                }
            }
            catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public void addData(WindparkData data) {
        stack.add(data);
    }

    public void shutdown() {
        sending = false;
    }
}
