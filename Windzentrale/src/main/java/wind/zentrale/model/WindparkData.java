package wind.zentrale.model;

import javax.xml.bind.annotation.*;

/**
 * @author Kacper Urbaniec
 * @version 21.01.2019
 */
@XmlRootElement(name = "Windpark")
@XmlType(propOrder={"windparkID", "data"})
public class WindparkData {

    @XmlElement(name = "WindparkID")
    private String windparkID;

    @XmlElement(name = "WindengineData")
    private WindengineData[] data;

    public WindparkData(String windparkID, WindengineData[] data) {
        this.windparkID = windparkID;
        this.data = data;
    }

    public WindparkData() {}

    public String getWindparkID() {
        return windparkID;
    }

    public WindengineData[] getWindpark() {
        return data;
    }

    public void setWindparkID(String windparkID) {
        this.windparkID = windparkID;
    }

    public void setWindpark(WindengineData[] data) {
        this.data = data;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("WindparkID: ").append(windparkID).append("\n");
        for(WindengineData d: data) {
            str.append(d.toString()).append("\n");
        }
        return str.toString();
    }
}