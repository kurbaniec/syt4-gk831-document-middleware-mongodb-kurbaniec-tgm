package wind.zentrale.model;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Kacper Urbaniec
 * @version 21.01.2019
 */
@XmlRootElement(name = "windzentrale")
public class WindzentraleData {


    private HashMap<String, ArrayList<WindengineData>> zentrale;

    public WindzentraleData(HashMap<String, ArrayList<WindengineData>> zentrale) {
        this.zentrale = zentrale;
    }

    public WindzentraleData() {}

    @XmlJavaTypeAdapter(MapAdapter.class)
    @XmlElement(name="Windparks")
    public HashMap<String, ArrayList<WindengineData>> getZentrale() {
        return zentrale;
    }

    public void setZentrale(HashMap<String, ArrayList<WindengineData>> zentrale) {
        this.zentrale = zentrale;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("Windzentrale\n");
        for(String key: zentrale.keySet()) {
            str.append("Windpark ").append(key).append("\n");
            for(WindengineData w: zentrale.get(key)) {
                str.append(w.toString()).append("\n");
            }
        }
        return str.toString();
    }

}