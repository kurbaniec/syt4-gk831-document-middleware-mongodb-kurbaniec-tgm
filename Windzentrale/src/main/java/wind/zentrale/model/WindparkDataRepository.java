package wind.zentrale.model;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author Kacper Urbaniec
 * @version 05.03.2019
 */
public interface WindparkDataRepository extends MongoRepository<WindparkData, String> {

    public List<WindparkData> findByWindparkID(String windparkID);

}
