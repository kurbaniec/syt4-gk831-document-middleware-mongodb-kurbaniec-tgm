package wind.zentrale.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;
import wind.zentrale.model.WindparkData;
import wind.zentrale.model.WindparkDataRepository;
import wind.zentrale.mongo.MongoSaver;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

/**
 * Startet einen MongoSaver-Thread und alle benötigten RestConsumer-Threads.
 * <br>
 * Außerdem werden am Anfang alle in der Datenbank gespeicherten Daten
 * ausgegben.
 * <br>
 * @author Kacper Urbaniec
 * @version 08.03.2019
 */
@Service
@Configurable
public class RestService {

    @Autowired
    ApplicationContext context;

    @Autowired
    TaskExecutor taskExecutor;

    @Autowired
    private WindparkDataRepository repository;

    @Value("${windpark.count}")
    private String windCountString;

    private int windCount;

    private ArrayList<RestConsumer> consumerThreads = new ArrayList<>();

    @PostConstruct
    public void executeAsynchronously() {
        // Alle in der Datenbank gespeicherten Daten ausgeben
        System.out.println("#####################");
        System.out.println("Saved Data:\n");
        for(WindparkData d : repository.findAll()) {
            System.out.println(d);
        }
        System.out.println("#####################\n\n");

        // Threads starten
        windCount = Integer.parseInt(windCountString);

        System.out.println("Starting MongoSaver");
        MongoSaver mongo = context.getBean(MongoSaver.class);
        taskExecutor.execute(mongo);

        System.out.println("Starting " + windCount + " RestConsumer-Thread(s)");
        int port = 8080;

        for(int i = 0; i < windCount; i++) {
            RestConsumer consumer = context.getBean(RestConsumer.class, mongo, port);
            consumerThreads.add(consumer);
            // Thread ausführen
            taskExecutor.execute(consumer);
            port += 100;
        }
    }
}

