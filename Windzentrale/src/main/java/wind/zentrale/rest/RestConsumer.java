package wind.zentrale.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import wind.zentrale.model.WindparkData;
import wind.zentrale.mongo.MongoSaver;


/**
 * Holt von einem Windpark alle drei Sekunden Daten
 * die in der Windpark-Zentrale gespeichert werden sollen
 * mittels {@link MongoSaver}.
 * <br>
 * @author Kacper Urbaniec
 * @version 8.03.2019
 */
@Component
@Scope("prototype")
public class RestConsumer implements Runnable{
    @Autowired
    private ApplicationContext context;

    @Autowired
    private RestService service;

    @Autowired
    private MongoRepository repository;

    private boolean sending =  true;

    private ObjectMapper mapper;

    private MongoSaver mongo;

    private int port;

    public RestConsumer(MongoSaver mongo, int port) {
        this.mongo = mongo;
        this.port = port;
    }

    @Override
    public void run() {
        while (sending) {
            RestTemplate rest = new RestTemplate();
            try {
                WindparkData data =
                        rest.getForObject(
                                "http://localhost:"+port+"/windpark/data_json",
                                WindparkData.class
                        );
                mongo.addData(data);
                System.out.println(data.toString());
                Thread.sleep(3000);
            }
            catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public void shutdown() {
        sending = false;
    }
}
