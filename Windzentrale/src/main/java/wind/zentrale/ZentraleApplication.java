package wind.zentrale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.client.RestTemplate;
import wind.zentrale.model.WindparkData;
import wind.zentrale.model.WindparkDataRepository;

@SpringBootApplication
public class ZentraleApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context =
                SpringApplication.run(ZentraleApplication.class, args);
    }

}
