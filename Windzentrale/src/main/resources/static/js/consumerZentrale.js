//first add an event listener for page load
document.addEventListener( "DOMContentLoaded", get_json_data, false ); // get_json_data is the function name that will fire on page load

//this function is in the event listener and will execute on page load
function get_json_data(){
    // Relative URL of external json file
    var json_url = 'http://localhost:9999/windzentrale/data_json';

    //Build the XMLHttpRequest (aka AJAX Request)
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {//when a good response is given do this

            var data = JSON.parse(this.responseText); // convert the response to a json object
            append_json(data);// pass the json object to the append_json function
        }
    };
    //set the request destination and type
    xmlhttp.open("POST", json_url, true);
    //set required headers for the request
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // send the request
    xmlhttp.send(); // when the request completes it will execute the code in onreadystatechange section
}

//this function appends the json data to the table 'winddata'
function append_json(data){
    var table = document.getElementById('winddata');
    for (park in data["zentrale"]) {
        for(engine in data["zentrale"][park]) {
            var tr = document.createElement('tr');
            var tmp = data["zentrale"][park][engine];
            tr.innerHTML =
                '<td>' + park + '</td>' +
                '<td>' + tmp["timestamp"] + '</td>' +
                '<td>' + tmp["windengineID"] + '</td>' +
                '<td>' + tmp["windspeed"] + " " + tmp["unitWindspeed"] + '</td>' +
                '<td>' + tmp["rotationspeed"] + " " + tmp["unitRotationspeed"] + '</td>' +
                '<td>' + tmp["bladeposition"] + " " + tmp["unitBladeposition"] + '</td>' +
                '<td>' + tmp["temperature"] + " " + tmp["unitTemperature"] + '</td>' +
                '<td>' + tmp["power"] + " " + tmp["unitPower"] + '</td>' +
                '<td>' + tmp["blindpower"] + " " + tmp["unitBlindpower"] + '</td>';
            table.appendChild(tr);
        }
    }

}